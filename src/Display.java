public class Display {
    public static void salam (){
        String pesan = "selamat datang di menara sejati";
        System.out.println(pesan);
    }

    public static void sayHello (String nama) {
        System.out.println("hello " + nama);
    }

    public static void infoLantai(int lantai) {
        String pesan = "saat ini lift berada di lantai : " +  lantai;
        System.out.println(pesan);
    }

    public static void naik(int lantaiNow){
        String pesan = "lift naik ke lantai " + (lantaiNow + 1);
        System.out.println(pesan);
    }

    public static void turun(int lantaiNow){
        String pesan = "lift turun ke lantai " + (lantaiNow - 1);
        System.out.println(pesan);
    }

    public static void pesanInputNama (){
        String pesan = "Tolong masukan nama anda [minimal 5 karakter] : ";
        System.out.println(pesan);
    }

    public static void errorInputNama (){
        String pesan = "Nama anda harus lebih dari 4 karakter !";
        System.out.println(pesan);
    }

    public static void errorInputLantai (){
        String pesan = "Lantai harus berada dalam rentang 1 - 10";
        System.out.println(pesan);
    }

    public static void pesanInputPenjemutan (){
        String pesan = "Silahkan masukan lantai penjemputan (1-10)";
        System.out.println(pesan);
    }

    public static void pesanInputTujuan(){
        String pesan = "Silahkan masukan lantai tujuan (1-10)";
        System.out.println(pesan);
    }

    public static void mohonTunggu (){
        String pesan = "Mohon menunggu, lift datang menjemput anda";
        System.out.println(pesan);
    }

    public static void sampaiPenjemputan (){
        String pesan = "lift sudah sampai di lokasi penjemputan, pintu lift terbuka";
        System.out.println(pesan);
    }

    public static void sampaiTujuan (){
        String pesan = "lift sudah sampai di tujuan, pintu lift terbuka";
        System.out.println(pesan);
    }

    public static void silahkanMasuk (){
        String pesan = "silhakan masuk, Anda akan diantar ke laantai tujuan";
        System.out.println(pesan);
    }

    public static void terimakasih (){
        String pesan = "Terima kasih, Anda sudah sampai di lantai tujuan";
        System.out.println(pesan);
    }

    public static void pesanDefault (){
        String pesan = "pesan default / tidak terdaftar";
        System.out.println(pesan);
    }
}
