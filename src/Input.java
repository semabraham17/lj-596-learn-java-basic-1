import java.util.Scanner;

public class Input {
    public static int readPenjemputan() {
        Scanner input = new Scanner(System.in);
        return Integer.parseInt(input.nextLine());
    }

    public static int readTujuan() {
        Scanner input = new Scanner(System.in);
        return Integer.parseInt(input.nextLine());
    }

    public static String readNama() {
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }
}
