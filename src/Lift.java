public class Lift {
    private int lantai;

    public Lift( int lantaiAwal) {
        this.lantai = lantaiAwal;
    }
    public void jemput (int penjemputan){

        Display.infoLantai(this.lantai);
        if (penjemputan != this.lantai) {
            Display.mohonTunggu();
            moveTo(penjemputan);
            Display.sampaiPenjemputan();
        }
        Display.silahkanMasuk();
    }

    public void antar(int tujuanB){
        moveTo(tujuanB);
        Display.sampaiTujuan();
        Display.terimakasih();
    }

    private void moveTo (int lantaiTujuan){
        while (this.lantai != lantaiTujuan) {
            if (this.lantai < lantaiTujuan) moveUp();
            else moveDown();
        }
    }

    private void moveUp (){
        Display.naik(this.lantai);
        this.lantai += 1;
    }

    private void moveDown (){
        Display.turun(this.lantai);
        this.lantai -= 1;
    }

}
