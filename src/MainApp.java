public class MainApp {

    public static void main(String[] args) {

        String nama = "";
        int lantaiAwal = new java.util.Random().nextInt(10) + 1;

        Lift lift1 = new Lift(lantaiAwal);

        Display.salam();
        Display.pesanInputNama();
        nama = Input.readNama();

        while(nama.length() < 5){
            Display.errorInputNama();
            Display.pesanInputNama();

            nama = Input.readNama();
        }

        Display.sayHello(nama);

        Display.pesanInputPenjemutan();
        int penjemputan = Input.readPenjemputan();
        while(penjemputan > 10 || penjemputan < 1){
            Display.errorInputLantai();
            Display.pesanInputPenjemutan();
            penjemputan = Input.readPenjemputan();
        }

        Display.pesanInputTujuan();
        int tujuan = Input.readTujuan();
        while(tujuan > 10 || tujuan < 1){
            Display.errorInputLantai();
            Display.pesanInputTujuan();

            tujuan = Input.readTujuan();
        }

        if (tujuan != penjemputan){
            lift1.jemput(penjemputan);
            lift1.antar(tujuan);
        }
        else System.out.println("Anda tidak perlu naik lift");
    }
}
